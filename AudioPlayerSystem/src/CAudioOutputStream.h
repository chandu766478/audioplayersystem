/*
 * CAudioOutputStream.h
 *
 *  Created on: Nov 17, 2019
 *      Author: asifn
 */


#ifndef CAUDIOOUTPUTSTREAM_H_
#define CAUDIOOUTPUTSTREAM_H_

#include <iostream>
#include <stdlib.h>

#include "portaudio.h"

enum CAUDIO_STREAM_STATE{CAUDIO_S_READY,CAUDIO_S_NOT_READY, CAUDIO_S_PLAYING};

class CAudioOutputStream
{
	CAUDIO_STREAM_STATE AudioStreamState;
	PaStreamParameters m_outputParameters; 	// desired configuration of the stream
	PaStream* m_stream;					 	// we'll get the address of the stream from Pa_OpenStream
	PaError m_error;						// PortAudio specific error values
	long m_samplerate;						// sample Rate
	int m_channelcnt;						// Number of channels
	int m_blockcnt;
	float* m_databuf;
	long m_framesPerBlock;
	long m_sbufsize;



public:
	CAudioOutputStream();
	virtual ~CAudioOutputStream();
	void open(long samplerate,int channelcnt, int blockcnt, float* databuf, long sbufsize);
	void close();
	void start();
	void resume();
	void stop();
	void pause();
};

#endif /* CAUDIOOUTPUTSTREAM_H_ */
