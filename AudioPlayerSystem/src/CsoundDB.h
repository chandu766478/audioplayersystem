/*
 * CsoundDB.h
 *
 *  Created on: Jan 19, 2020
 *      Author: CHANDU-PM
 */

#ifndef SRC_CSOUNDDB_H_
#define SRC_CSOUNDDB_H_

#include<string>
using namespace std;
#include"CDatabase.h"

class CsoundDB: public CDatabase
{
private:
	int m_soundflid;
	char m_flfolder[DB_TEXTLEN];
	char m_flname[DB_TEXTLEN];
	int m_sfrequency;
	int m_nchannels;
	int m_soundscount;



public:
	CsoundDB();
	virtual ~CsoundDB();
	const char* getFilefolder() const;
	const char* getFilename() const;
	int getNoofchannels() const;
	int getSamplingfrequency() const;
	int getSoundfileid() const;
	bool selectAllSounds();
	bool selectFsBySfid(int soundfieldid);
	bool selectAllSoundColumns();
	bool insertSound(string file_folder, string file_name, int sampling_frequency, int no_of_channels);
	bool NoOfSoundFiles();
	int getSoundsCount();
	bool findSoundsCount();
};


#endif /* SRC_CSOUNDDB_H_ */
