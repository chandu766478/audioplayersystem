
/*
 * CsoundDB.cpp
 *
 *  Created on: Jan 19, 2020
 *      Author: CHANDU-PM
 */
#include "CsoundDB.h"
#include<iostream>


CsoundDB::CsoundDB()
{
	// TODO Auto-generated constructor stub
	m_soundflid = 0;
	m_flfolder[0] = 0;
	m_flname[0] = 0;
	m_sfrequency = 0;
	m_nchannels=2;
	m_soundscount=0;
}

const char* CsoundDB::getFilefolder() const
{
	return m_flfolder;
}

const char* CsoundDB::getFilename() const
{
	return m_flname;
}

int CsoundDB::getNoofchannels() const
{
	return m_nchannels;
}

int CsoundDB::getSamplingfrequency() const
{
	return m_sfrequency;
}

int CsoundDB::getSoundfileid() const
{
	return m_soundflid;
}
int CsoundDB::getSoundsCount() {

	return m_soundscount;
}
CsoundDB::~CsoundDB()
{
	// TODO Auto-generated destructor stub
}

bool CsoundDB::selectAllSounds()
{
	if(m_eState != DB_S_CONNECTED)
		return false;

	string sSQL;
	sSQL = "SELECT * FROM soundata;";
	if(_executeSQLStmt(sSQL, "selectAllSounds", true))
	{
		SQLBindCol(m_hStmt, 1, SQL_C_LONG, &m_soundflid, 0, &m_info);
		SQLBindCol(m_hStmt, 2, SQL_C_CHAR, m_flfolder, DB_TEXTLEN, &m_info);
		SQLBindCol(m_hStmt, 3, SQL_C_CHAR, m_flname, DB_TEXTLEN, &m_info);
		SQLBindCol(m_hStmt, 4, SQL_C_LONG, &m_sfrequency, 0, &m_info);
		SQLBindCol(m_hStmt, 5, SQL_C_LONG, &m_nchannels, 0, &m_info);
		return true;
	}
	else
		return false;
}

bool CsoundDB::selectFsBySfid(int soundfileid)
{
if(m_eState != DB_S_CONNECTED)
		return false;

	string sSQL;
	sSQL = "SELECT soundfolder,soundfilename,soundfs,soundchannels FROM soundata WHERE soundfileid=" +to_string(soundfileid);
	if(true ==_executeSQLStmt(sSQL, "select fs given sound file id", true))
	{
				SQLBindCol(m_hStmt, 1, SQL_C_CHAR, m_flfolder, DB_TEXTLEN, &m_info);
				SQLBindCol(m_hStmt, 2, SQL_C_CHAR, m_flname, DB_TEXTLEN, &m_info);
				SQLBindCol(m_hStmt, 3, SQL_C_LONG, &m_sfrequency, 0, &m_info);
				SQLBindCol(m_hStmt, 4, SQL_C_LONG, &m_nchannels, 0, &m_info);
		bool a = fetch();
		closeQuery();
		return a;
	}
	return false;
}



bool CsoundDB::selectAllSoundColumns()
{
	if(m_eState != DB_S_CONNECTED)
		return false;

	string sSQL;
	sSQL="SELECT * FROM soundata;";
		if( _executeSQLStmt(sSQL, "selectAllSoundColumns ", true) )
		{
			SQLBindCol( m_hStmt, 1, SQL_C_LONG, &m_soundflid, 0, &m_info );
			SQLBindCol( m_hStmt, 2, SQL_C_CHAR,m_flfolder, DB_TEXTLEN, &m_info );
			SQLBindCol( m_hStmt, 3, SQL_C_CHAR, m_flname, DB_TEXTLEN, &m_info );
			SQLBindCol( m_hStmt, 4, SQL_C_LONG, &m_sfrequency, 0, &m_info );
			SQLBindCol( m_hStmt, 5, SQL_C_LONG, &m_nchannels, 0, &m_info );
			return true;
		}
	else
		return false;
}


bool CsoundDB::insertSound(string file_folder, string file_name,
		int sampling_frequency, int no_of_channels) {

	if(m_eState!=DB_S_CONNECTED)
			return false;

		bool bPrintStmts=true;

		string sSQL;
		// insertdata into soundfiles
		sSQL= "INSERT INTO soundata (soundfolder,soundfilename,soundfs,soundchannels) VALUES (\'" + file_folder + "\',\'" + file_name + "\', "+ to_string(sampling_frequency) + "," + to_string(no_of_channels) +");";
		_executeSQLStmt(sSQL, "insertSoundFileData",bPrintStmts);

		return true;
}

bool CsoundDB::NoOfSoundFiles() {

	if(m_eState!=DB_S_CONNECTED)
			return false;

	string sSQL;
		sSQL="select count(*) from soundata;";
		if( _executeSQLStmt(sSQL, "SelectAllSoundFiles ", true) )
		{
			SQLBindCol( m_hStmt, 1, SQL_C_LONG, &m_soundscount, 0, &m_info );
			return true;
		}
		else
			return false;
}

bool CsoundDB::findSoundsCount() {

	if(m_eState!=DB_S_CONNECTED)
				return false;

			string sSQL;
			sSQL="SELECT count(*) FROM soundata;";
			if(_executeSQLStmt(sSQL, "SelectCountOfSoundfiles", true))
			{
				SQLBindCol( m_hStmt, 1, SQL_C_LONG, &m_soundscount, 0, &m_info );
				return true;
			}
			else
				return false;
}
