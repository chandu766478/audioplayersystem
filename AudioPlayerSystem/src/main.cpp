/*
 * main.cpp
 *
 *  Created on: 09.01.2020
 *      Author: Wirth
 */
////////////////////////////////////////////////////////////////////////////////
#include<stdio.h>
#include<iostream>
using namespace std;

// Header

#include "CAudioPlayerController.h"
#include "CFile.h"
#include "CFilter.h"
#include "CASDDException.h"
#include "CDatabase.h"
#include "CFilterDB.h"
#include "CsoundDB.h"
#include "CSoundFile.h"
#include "CAudioOutputStream.h"
#include "CFilter.h"
#include "CsoundDB.h"

//void button_test_1();

int main (void)
{
	setvbuf(stdout, NULL, _IONBF, 0);
	CAudioPlayerController myController; 	// create the controller
	CUserInterfaceCmdIOW ui;				// create an user interface object for the controller (may be replaced by another ui type)
	ui.showMessage("Lab05 Prep started.");
	myController.run(&ui);					// run the controller

	ui.showMessage("Bye!");
	return 0;
}





